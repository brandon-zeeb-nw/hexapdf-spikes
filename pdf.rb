require 'hexapdf'
require 'pry'

sample_text = 'i am good at making you happy, but if you are really trying to get me to stop playing your game, you are going to be better off if you find a way to make me like it. You were not doing a very good job of it, and neither was I, so I was having some fun with this game, getting some great games out of it and enjoying myself. So, I tried to take that pleasure back by trying to make you happy."'


pdf = HexaPDF::Document.open('cas.pdf', config: {'page.default_media_box' => :A4}) do |doc|
    radius = 20
    page = doc.pages[0]

    page_width = page.box.width
    page_height = page.box.height

    box_x = 0.01 * page_width
    box_y = 0.99 * page_height

    box_width = 0.1 * page_width
    box_height = 0.5 * page_height

    canvas = page.canvas(type: :overlay)
    canvas.font("Times", size: 10, variant: :bold)

    tf = HexaPDF::Layout::TextFragment.create(sample_text, font: doc.fonts.add("Times"))    
    tl = HexaPDF::Layout::TextLayouter.new

    box = HexaPDF::Layout::Box.create(
      width: box_width, height: box_height, content_box: false,
      border: {width: 0, style: :solid},
      background_color: [240, 120, 120])
    box

    tl.style.align(:left).valign(:top)
    fitted = tl.fit([tf], box_width, box_height)
    
    box.draw(canvas, box_x, box_y - fitted.height)
    fitted.draw(canvas, box_x, box_y)
    #canvas.stroke_color(128, 0, 0).rectangle(box_x, box_y, box_width, -box_height).stroke

    doc.write('output.pdf', optimize: true)
end